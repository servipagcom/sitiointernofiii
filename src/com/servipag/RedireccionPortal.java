package com.servipag;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cl.servipag.utilitarios.LoggingUtils;
import cl.servipag.utilitarios.Redireccion;
import com.ibm.wps.logging.LogManager;
import com.ibm.wps.logging.Logger;
public class RedireccionPortal extends HttpServlet {
	private static Logger logger = LogManager.getLogManager().getLogger(
			RedireccionPortal.class);
	private static final long serialVersionUID = 1L;
    public RedireccionPortal() {
        super();
       
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoggingUtils.debug(logger, " DO GET : RedireccionPortal ");
		Cookie[] cookies = request.getCookies();	
			for (int i = 0; i < cookies.length; i++) 
			{
				if (cookies[i].getName().equals("ServipagSsnsPrtl")) 
				{
//					cookies[i].setMaxAge(0);
					String retorno = cookies[i].getValue();	
		            ByteBuffer bbuf = ByteBuffer.wrap(retorno.getBytes());
		            CharBuffer aux = Charset.forName("UTF-8").decode(bbuf);
		            retorno = aux.toString(); 
	            	cookies[i].setPath("/");
	            	cookies[i].setValue(retorno);  
	                response.addCookie(cookies[i]);
//		            Cookie cookie = new Cookie("ServipagSsnsPrtl", retorno);
//		            cookie.setPath("/");
//		            response.addCookie(cookie);
				}
			}
			
		String pagina = null;
		String portlet = null;	
		String CANAL = request.getParameter("canal");
		String serverContext ="";		
		if(CANAL.equals("1")){			
			LoggingUtils.debug(logger, "Redirecciona a comprobante pago Portal");
			pagina ="ComprobantePago";
			portlet="ComprobanteP";			
		}
		if(CANAL.equals("99")){
			LoggingUtils.debug(logger, "Redirecciona a comprobante pago Movil");
			pagina ="ComprobantePagoMovil";
			portlet="mv_comprobante";					
		}	
			Redireccion.redirige(pagina, portlet, null, validaSesion(request), serverContext, request, response, logger);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	private boolean validaSesion(HttpServletRequest request)
	{
		boolean resultado=false;	
		Cookie[] cookies = request.getCookies();	
		if (cookies != null) 
		{
			for (int i = 0; i < cookies.length; i++) 
			{
				if (cookies[i].getName().equals("ServipagSsnsPrtlD")) 
				{
					resultado=true;
				}
			}
		}
		return resultado;
	}
		
}
